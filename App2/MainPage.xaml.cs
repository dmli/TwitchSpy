﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Diagnostics;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Notifications;
using Microsoft.Toolkit.Uwp.Notifications; // Notifications library
using Microsoft.QueryStringDotNET; // QueryString.NET
using Newtonsoft.Json; // JSON
using Newtonsoft.Json.Linq; // JSON LINQ

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 

        // TO-DO: start-up, alarm-mode/priority mode notifications
    public sealed partial class MainPage : Page
    {
        DispatcherTimer dispatcherTimer;
        IList<string> streamList;
        List<bool> alreadyLive = new List<bool>();
        string fileName = "URLs.txt";
        public MainPage()
        {
            this.InitializeComponent();
            ReadConfig();
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Interval = new TimeSpan(0, 1, 0);
            dispatcherTimer.Start();
            dispatcherTimer.Tick += Tick;
        }

        private void Tick(object sender, object e)
        {
            Poll();
        }

        private async void WriteConfig(string URL)
        {
            // Check that there is a username in the URL
            int index = URL.LastIndexOf('/');
            string username = URL.Substring(index + 1);
            if (username.Length != 0)
            {
                Windows.Storage.StorageFolder storageFolder =
                    Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile URLFile =
                    await storageFolder.CreateFileAsync(fileName,
                        Windows.Storage.CreationCollisionOption.OpenIfExists);
                await Windows.Storage.FileIO.AppendTextAsync(URLFile, URL + Environment.NewLine);
            }
            ReadConfig();
        }

        private async void ReadConfig()
        {
            Windows.Storage.StorageFolder storageFolder =
                Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile URLFile =
                await storageFolder.CreateFileAsync(fileName,
                    Windows.Storage.CreationCollisionOption.OpenIfExists);
            streamList = await Windows.Storage.FileIO.ReadLinesAsync(URLFile);
            for (int i = 0; i < streamList.Count; i++)
            {
                alreadyLive.Add(false);
                if (!listMenu.Items.Contains(streamList[i]))
                {
                    listMenu.Items.Add(streamList[i]);
                }      
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string URL = textBox.Text;
            string TwitchURL = "www.twitch.tv/";

            if (URL.Contains(TwitchURL))
            {
                textBox.Text = "";
                WriteConfig(URL);
            }
            else
            {
                textBox.Text = "";
                textBox.PlaceholderText = "Not a valid URL. URL must contain www.twitch.tv/username.";
            }
        }

        private async void Remove_Click(object sender, RoutedEventArgs e)
        {
            string removeThis = "";
            try
            {
                removeThis = listMenu.SelectedValue.ToString();
            }
            catch (Exception E)
            {
                Console.WriteLine(E.ToString());
                return;
            }
            int index = listMenu.SelectedIndex;
            listMenu.Items.RemoveAt(index);
            streamList.Remove(removeThis);

            Windows.Storage.StorageFolder storageFolder =
                Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile URLFile =
                await storageFolder.CreateFileAsync(fileName,
                    Windows.Storage.CreationCollisionOption.OpenIfExists);
            await Windows.Storage.FileIO.WriteLinesAsync(URLFile, streamList);
        }

        private async void Poll()
        {
            for (int i = 0; i < streamList.Count; i++)
            {
                int index = streamList.ElementAt(i).LastIndexOf('/');
                string username = streamList.ElementAt(i).Substring(index + 1);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Client-ID", "kepqg6a9cqu81x1uhgc6ag75mqak4y");
                    string host = "https://api.twitch.tv/kraken/streams/";                   
                    string Url = host + username;
                    HttpResponseMessage response = await client.GetAsync(Url);
                    if (response.IsSuccessStatusCode)
                    {
                        string result = await response.Content.ReadAsStringAsync();
                        JToken jsonResult = JToken.Parse(result);
                        //JArray jsonArray = JArray.Parse(result); // If reply is an array
                        //Notify(jsonArray[0]);

                        if (jsonResult["stream"].HasValues && !alreadyLive[i] && jsonResult["stream"]["stream_type"].ToString() == "live")
                        {
                            Notify(jsonResult);
                            alreadyLive[i] = true;
                        }
                        if (!jsonResult["stream"].HasValues)
                        {
                            alreadyLive[i] = false;
                        }
                    }
                }
            }
            

        }
        private void Notify(JToken data)
        {
            string title = data["stream"]["channel"]["display_name"].ToString() + " is streaming " + data["stream"]["game"].ToString();
            string content = data["stream"]["channel"]["status"].ToString();
            string image = data["stream"]["preview"]["large"].ToString();
            string logo = data["stream"]["channel"]["logo"].ToString();

            // Toast visuals
            ToastVisual visual = new ToastVisual()
            {
                BindingGeneric = new ToastBindingGeneric()
                {
                    Children =
                {
                    new AdaptiveText()
                {
                    Text = title
                },

                new AdaptiveText()
                {
                    Text = content
                },

                new AdaptiveImage()
                {
                    Source = image
                }
                },

                    AppLogoOverride = new ToastGenericAppLogo()
                    {
                        Source = logo,
                        HintCrop = ToastGenericAppLogoCrop.Circle
                    }
                }
            };

            ToastContent toastContent = new ToastContent()
            {
                Visual = visual,
                Launch = data["stream"]["channel"]["url"].ToString(),
                ActivationType = ToastActivationType.Protocol,
                Audio = new ToastAudio() { Src = new Uri("ms-appx:///Assets/smw_kick.wav") }
            };

            var toast = new ToastNotification(toastContent.GetXml());
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }
    }
}
